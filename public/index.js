var w = 1500,
    h = 1100,
    radius = d3.scale.log().domain([0, 312000]).range(["10", "50"]);

var vis = d3.select("#graph-div").append("svg:svg")
    .attr("width", w)
    .attr("height", h);

d3.json("G.json", function (data) {
    var force = self.force = d3.layout.force()
        .nodes(data.nodes)
        .links(data.links)
        .distance(1000)
        .size([w, h])
        .on("tick", tick)
        .start();

    var drag = force.drag()
        .on("dragstart", dragstart);

    vis.append("defs").append("marker")
        .attr("id", "circle")
        .attr("refX", 1.5) /*must be smarter way to calculate shift*/
        .attr("refY", -1.5)
        .attr("markerWidth", 15)
        .attr("markerHeight", 15)
        .attr("orient", "auto")
        .attr('viewBox', '-6 -6 12 12')
        .append("path")
        .attr("d", "M 0, 0  m -5, 0  a 5,5 0 1,0 10,0  a 5,5 0 1,0 -10,0");

    var path = vis.append("g").selectAll("path")
          .data(force.links())
          .enter().append("path")
          .on("dblclick", dblclickPath)
          .attr("class", function (d) { return "link"; })
          .attr("stroke-width", 10);

    var node = vis.selectAll("g.node")
        .data(data.nodes)
        .enter().append("svg:g")
        .attr("class", function (d) {
            return "node-rectangle node";
        })
        .call(drag);

    d3.selectAll(".node-rectangle").append("rect")
        .attr("width", 100)
        .attr("height", 200)
        .attr("class", function (d) {
            return "node";
        });

    var images = node.append("svg:image")
        .attr("xlink:href", function (d) { return d.img; })
        .attr("x", function (d) { return -25; })
        .attr("y", function (d) { return -25; })
        .attr("height", 300)
        .attr("width", 150)
        .on("dblclick", dblclick);

    images.on('click', function (d) {
        // TODO: Mandar al detalle
    }).on('mouseenter', function () {
        // select element in current context
        d3.select(this)
            .transition()
            .attr("x", function (d) { return -60; })
            .attr("y", function (d) { return -60; })
            .attr("height", 350)
            .attr("width", 170);
    }).on('mouseleave', function () {
        d3.select(this)
            .transition()
            .attr("x", function (d) { return -25; })
            .attr("y", function (d) { return -25; })
            .attr("height", 300)
            .attr("width", 150);
    })

    var text = node.append("svg:text")
        .attr("class", "nodetext")
        .attr("dx", 0)
        .attr("dy", ".35em")
        .attr("x", -47)
        .attr("y", -47)
        .attr("text-anchor", "middle")
        .text(function (d) {
            return d.title
        });

    function dragstart(d) {
        d3.select(this).classed("fixed", d.fixed = true);
    }

    // Use elliptical arc path segments to doubly-encode directionality.
    function tick() {
        path.attr("d", linkArc);
        node.attr("transform", transform);
    }


    function dblclick(d) {
        console.log(d);
        if (d.link) {
            window.location.href = d.link;
        } else {
            alert('Este nodo no tiene detalle');
        }
    }

    function dblclickPath(d) {
        console.log(d);
        if (d.link) {
            window.location.href = d.link;
        } else {
            alert('Esta transición no tiene detalle');
        }
    }  

    function linkArc(d) {
        var dx = d.target.x - d.source.x,
            dy = d.target.y - d.source.y,
            dr = Math.sqrt(dx * dx + dy * dy);
        return "M" + (d.source.x) + "," + (d.source.y + 150) + "A" + dr + "," + dr + " 0 0,1 " + (d.target.x - 25) + "," + (d.target.y + 150);
    }

    function transform(d) {
        return "translate(" + d.x + "," + d.y + ")";
    }
});